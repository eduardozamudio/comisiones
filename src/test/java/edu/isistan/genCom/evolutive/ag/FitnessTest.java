/**
 *     This file is part of GenCom.
 *
 *     GenCom is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     GenCom is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GenCom.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * 
 */
package test.java.edu.isistan.genCom.evolutive.ag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import main.java.edu.isistan.genCom.evolutive.Evolutive;
import main.java.edu.isistan.genCom.evolutive.ag.functions.CreatorIndependence;
import main.java.edu.isistan.genCom.evolutive.ag.functions.CreatorKPPneg;
import main.java.edu.isistan.genCom.evolutive.ag.functions.CreatorKPPpos;
import main.java.edu.isistan.genCom.evolutive.ag.functions.FFCreator;
import main.java.edu.isistan.genCom.evolutive.ag.functions.FitnessFunction;
import main.java.edu.isistan.genCom.redSocial.Investigador;
import main.java.edu.isistan.genCom.redSocial.RedSocial;
import main.java.edu.isistan.genCom.util.Entropy;

/**
 * @author eduardo
 *
 */
public class FitnessTest {
	private RedSocial red;
	private Evolutive evolutive;
	private FitnessFunction fitnessFunctions;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.red = new RedSocial();
		this.evolutive = new Evolutive();
		
		this.fitnessFunctions = new FitnessFunction();
		
		List<FFCreator> creators = new ArrayList<>();
		
		creators.add(new CreatorKPPpos());
		creators.add(new CreatorKPPneg());
		creators.add(new CreatorIndependence());
//		creators.add(new CreatorFFGroup());
		
		for (FFCreator ffCreator : creators) {
			fitnessFunctions.set(ffCreator.getNombre(), ffCreator);
		}

		evolutive.setRedSocial(red);
	}

	@Test
	public void test() {
		red.actualizar();
		Set componentes = red.getComponentes();

		/* Reducir la red al componente más grande */
		Set<Investigador> greatestComp = null;
		int greatestCompSize = 0;

		for (Object object : componentes) {
			Set componente = (Set) object;

			if (componente.size() > greatestCompSize) {
				greatestComp = componente;
				greatestCompSize = componente.size();
			}
		}

		red.reducirA(greatestComp);

		/* Evalúa los fitness para las distintas comisiones */

		// Admisiones
		// 1473623131;" GODOY, DANIELA LIS "
		// 1290859243;" CHESNEVAR, CARLOS IVAN "
		// 1104138272;" KOFMAN, ERNESTO JAVIER "

		System.out.println("\nFitnesses for Admisions Committee:");

		Long[] admArray = { 1473623131L, 1290859243L, 1104138272L };
		List<Investigador> committeeAdm = red.getNodos(admArray);

		Map<String, Double> fitAdm = calcFitnesses(committeeAdm);

		// Reports
		// 525994944;" FRIAS, MARCELO FABIAN "
		// 1525459260;" MILONE, DIEGO HUMBERTO "
		// 1247627338;" UCHITEL, SEBASTIAN "
		// 1180867063;" ZUNINO SUAREZ, ALEJANDRO OCTAVIO "
		System.out.println("\nFitnesses for Reports Committee:");

		Long[] repArray = { 525994944L, 1525459260L, 1247627338L, 1180867063L };
		List<Investigador> committeeRep = red.getNodos(repArray);

		Map<String, Double> fitRep = calcFitnesses(committeeRep);

		// Fellowship
		// 145820629;" DIAZ PACE, JORGE ANDRES "
		// 2017159079;" FIGUEIRA, SANTIAGO "
		// 1488869003;" GONNET, SILVIO MIGUEL "
		// 565576377;" MAGUITMAN, ANA GABRIELA "
		// 1643880502;" RUFINER, HUGO LEONARDO "

		System.out.println("\nFitnesses for Fellowship Awards Committee:");

		Long[] felArray = { 145820629L, 2017159079L, 1488869003L, 565576377L, 1643880502L };
		List<Investigador> committeeFel = red.getNodos(felArray);

		Map<String, Double> fitFel = calcFitnesses(committeeFel);

		
		/* Calcula los pesos de los atributos mediante el método de Entropía */
		String[] functions = fitnessFunctions.getKeys();
		
		Object[] fitnesses = { fitAdm, fitRep, fitFel };

		double[][] p = new double[fitnesses.length][functions.length]; // n:committees,
																		// m:functions

		for (int i = 0; i < fitnesses.length; i++) {
			Map<String, Double> fit = (Map<String, Double>) fitnesses[i];

			for (int j = 0; j < functions.length; j++) {
				p[i][j] = fit.get(functions[j]);
			}
		}

		double[] weights = Entropy.getValues(p);

		
		// Imprime los pesos
		System.out.println("\nPesos de atributos (Entropía):");

		for (int i = 0; i < functions.length; i++) {
			System.out.printf("%s:\t%.10f\n", functions[i], weights[i]);
		}

	}

	/**
	 * Calculates the fitnesses for the committee
	 * 
	 * @param committee
	 * @return
	 */
	public Map<String, Double> calcFitnesses(List<Investigador> committee) {
		Map<String, Double> funcFit = new HashMap<>();
		
		String[] functions = fitnessFunctions.getKeys();
		
		for (String k : functions) {
			funcFit.put(k, fitnessFunctions.get(k).getInstance(red).getFitness(committee));
		}
		
		// Imprime los resultados
		for (Investigador i : committee) {
			System.out.println(i);
		}

		for (String k : funcFit.keySet()) {
			System.out.printf("%s:\t%s\n", k, funcFit.get(k));
		}

		return funcFit;
	}

}
