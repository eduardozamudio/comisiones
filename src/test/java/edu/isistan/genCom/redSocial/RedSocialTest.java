/**
 *     This file is part of GenCom.
 *
 *     GenCom is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     GenCom is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with GenCom.  If not, see <http://www.gnu.org/licenses/>.
 */
package test.java.edu.isistan.genCom.redSocial;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import main.java.edu.isistan.genCom.redSocial.RedSocial;

public class RedSocialTest {

	@Test
	public void testActualizar() {
		RedSocial red = new RedSocial();
		red.actualizar();
		
		boolean estaCargada = false;
		Integer nodos = red.getCantidadDeNodos();
		System.out.println("cantidad de nodos: "+String.valueOf(nodos));
		if(nodos > 0)
			estaCargada = true;
		assertTrue("Esperando que la red tenga nodos", estaCargada);
	

	}
	
	

}
